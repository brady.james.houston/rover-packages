# Mini-Project 3: Velocity Control
Kenny Bradley

Brady Houston

Taylor Jones

Rachel Wall

## Execution Instructions
### Installation Instructions
1. Ensure you are in the src directory of your catkin_ws
2. Put the rover.rosinstall in this src folder
3. Run: rosinstall . .rosinstall
4. Run: rosdep update
5. Run: rosdep install --from-paths . --ignore-src
6. Then go to the root of your catkin workspace and run: catkin_make

### Running Instructions
1. Source your workspace: source devel/setup.bash
2. Launch RVIZ and Teleop: roslaunch rover proj3.launch
3. This will launch RVIZ. Now place the 2D nav markers to watch the rover move

## Go-to-Goal Behavior
We updated the Go-to-Goal behavior to utilize Mecanum drive. Modifications were also made to cause the robot to orient to the goal orientation instead of towards the goal itself.
This project supports the SimpleNav 2D goals created by Rviz. 

## Updated Velocity Control
We used the same model as found in Mini-project 2. 

### Model Used
We used the forward drive kinematics built for mecanum wheels for this project. These kinematics are found at this url: http://robotsforroboticists.com/drive-kinematics/ .
The equations for each wheel are detailed below:

wheel_front_left = (1/WHEEL_RADIUS) * (linear.x – linear.y – (WHEEL_SEPARATION_WIDTH + WHEEL_SEPARATION_LENGTH)*angular.z);

wheel_front_right = (1/WHEEL_RADIUS) * (linear.x + linear.y + (WHEEL_SEPARATION_WIDTH + WHEEL_SEPARATION_LENGTH)*angular.z);

wheel_rear_left = (1/WHEEL_RADIUS) * (linear.x + linear.y – (WHEEL_SEPARATION_WIDTH + WHEEL_SEPARATION_LENGTH)*angular.z);

wheel_rear_right = (1/WHEEL_RADIUS) * (linear.x – linear.y + (WHEEL_SEPARATION_WIDTH + WHEEL_SEPARATION_LENGTH)*angular.z);


### Detailed Control Design Documentation
The control design was done similarly to HW 4 Problem 4. 
Vy was added to the diffeomorphism differential drive method and equations to allow the mecanum wheels to drive sideways.
This made it so there were three dimensions of state: vx, vy, and vtheta. 
The parameters we used are: 
```
A=3x3 zeros
B=3x3 Identity matrix
poles=[-1,-2,-3]
K=place(A,B,poles)=diagonal matrix with poles. 
x=[vx,vy,vtheta]
u=(A-BK)x
```
