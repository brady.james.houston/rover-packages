#include "dynamic_models/rover_velocity.h"
#define _USE_MATH_DEFINES
#include <math.h>

using namespace rover_kinematics;

Rover::Rover() {
    init();
}

Rover::~Rover(){

}

bool Rover::init(){
    // Initialize the odometry position
    odom_.pose.pose.position.x = 0.0;
    odom_.pose.pose.position.y = 0.0;
    odom_.pose.pose.position.z = 0.0;
    yaw_ = 0.0;
    odom_.pose.pose.orientation = tf::createQuaternionMsgFromYaw(yaw_);

    // Intialize the odometry twist
    linear_velocity_x_ = 0.0;
    linear_velocity_y_ = 0.0;
    angular_velocity_ = 0.0;
    linear_velocity_x_desired_ = 0.0;
    linear_velocity_y_desired_ = 0.0;
    angular_velocity_desired_ = 0.0;
    odom_.twist.twist.linear.x = 0.0;
    odom_.twist.twist.linear.y = 0.0;
    odom_.twist.twist.linear.z = 0.0;
    odom_.twist.twist.angular.x = 0.0;
    odom_.twist.twist.angular.y = 0.0;
    odom_.twist.twist.angular.z = 0.0;


    nh_.param("odom_frame", odom_.header.frame_id, std::string("odom"));
    nh_.param("base_frame", odom_.child_frame_id, std::string("base_link"));

    double pcov[36] = { 0.1,   0,   0,   0,   0, 0,
                          0, 0.1,   0,   0,   0, 0,
                          0,   0, 1e6,   0,   0, 0,
                          0,   0,   0, 1e6,   0, 0,
                          0,   0,   0,   0, 1e6, 0,
                          0,   0,   0,   0,   0, 0.2};
    memcpy(&(odom_.pose.covariance),pcov,sizeof(double)*36);
    memcpy(&(odom_.twist.covariance),pcov,sizeof(double)*36);

    // initialize publishers
    odom_pub_         = nh_.advertise<nav_msgs::Odometry>("odom", 100);

    // initialize subscribers
    cmd_accel_sub_  = nh_.subscribe("cmd_vel", 100,  &Rover::commandVelocityCallback, this);

    prev_update_time_ = ros::Time::now();
    return true;
}

bool Rover::update(){
    ros::Time time_now = ros::Time::now();
    ros::Duration step_time = time_now - prev_update_time_;
    prev_update_time_ = time_now;

    // odom
    updateOdometry(step_time);
    odom_.header.stamp = time_now;
    odom_pub_.publish(odom_);

    // tf
    geometry_msgs::TransformStamped odom_tf;
    updateTF(odom_tf);
    tf_broadcaster_.sendTransform(odom_tf);

    return true;
}

void Rover::commandVelocityCallback(const geometry_msgs::TwistConstPtr cmd_vel_msg){
    linear_velocity_x_desired_ = cmd_vel_msg->linear.x;
    linear_velocity_y_desired_ = cmd_vel_msg->linear.y;
    angular_velocity_desired_ = cmd_vel_msg->angular.z;
}

bool Rover::updateOdometry(ros::Duration diff_time){
    double dt = diff_time.toSec();

    // Calculate control (Note: Using the linear algebra Eigen3 library would simplify notation)
    double linear_acceleration_x = -(linear_velocity_x_-linear_velocity_x_desired_);
    double linear_acceleration_y = -2*(linear_velocity_y_-linear_velocity_y_desired_);

    double angular_acceleration = -3*(angular_velocity_-angular_velocity_desired_);
    
    // Use Euler integration for updating the odometry
    odom_.pose.pose.position.x = odom_.pose.pose.position.x
        + dt * linear_velocity_x_ * std::cos(yaw_)
        + dt * linear_velocity_y_ * std::cos(yaw_+M_PI/2);
    odom_.pose.pose.position.y = odom_.pose.pose.position.y
        + dt * linear_velocity_x_ * std::sin(yaw_)
        + dt * linear_velocity_y_ * std::sin(yaw_+M_PI/2);
    yaw_ = yaw_ + dt * angular_velocity_;
    odom_.pose.pose.orientation = tf::createQuaternionMsgFromYaw(yaw_);

    // Update the odometry twist
    odom_.twist.twist.linear.x = linear_velocity_x_;
    odom_.twist.twist.linear.y = linear_velocity_y_;
    odom_.twist.twist.angular.z = angular_velocity_;


    linear_velocity_x_ = linear_velocity_x_ + dt * linear_acceleration_x;
    linear_velocity_y_ = linear_velocity_y_ + dt * linear_acceleration_y;
    angular_velocity_ = angular_velocity_ + dt * angular_acceleration;

    return true;
}

void Rover::updateTF(geometry_msgs::TransformStamped& odom_tf){
    odom_tf.header = odom_.header;
    odom_tf.child_frame_id = odom_.child_frame_id;
    odom_tf.transform.translation.x = odom_.pose.pose.position.x;
    odom_tf.transform.translation.y = odom_.pose.pose.position.y;
    odom_tf.transform.translation.z = odom_.pose.pose.position.z;
    odom_tf.transform.rotation = odom_.pose.pose.orientation;
}

/*******************************************************************************
* Main function
*******************************************************************************/
int main(int argc, char* argv[])
{
  ros::init(argc, argv, "Rover_kinematics_node");
  Rover Rover;

  ros::Rate loop_rate(30);

  while (ros::ok())
  {
    Rover.update();
    ros::spinOnce();
    loop_rate.sleep();
  }

  return 0;
}
